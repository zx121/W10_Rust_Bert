# Rust Serverless Transformer Endpoint

## Requirements
- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint


## Hugging Face GPU Example of Translation

### Steps

1. Open the project in VSCode so that it can automatically prepare for the environment.
2. Verify the environment. Run `python verify-PyTorch.py`
![alt text](image.png)
3. Prepare for the libtorch environment and set the ENV
* `source install-libtorch.sh`
```bash
export LIBTORCH=./ld_lib/libtorch
# export LIBTORCH_INCLUDE=./ld_lib/libtorch
# export LIBTORCH_LIB=./ld_lib/libtorch
# export LD_LIBRARY_PATH=./ld_lib/libtorch/lib:$LD_LIBRARY_PATH

export LD_LIBRARY_PATH=${LIBTORCH}/lib:$LD_LIBRARY_PATH
```

3. Write a AWS Lambda function which serve as the functionality of translating Chinese to English
* `cargo new translate`, and cd into it, fully working GPU Hugging Face Translation CLI in Rust
```rust
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let body = event.body();
    let s = std::str::from_utf8(body).expect("invalid utf-8 sequence");

    let item = match from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Body not provided correctly: ".to_string() + &err.to_string() + "\n").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let model = TranslationModelBuilder::new()
        .with_source_languages(vec![Language::Chinese])
        .with_target_languages(vec![Language::English])
        .create_model()?;

    let input_text = item.text;
    let output_text = model.translate(&[input_text], None, Language::English)?;

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/plain")
        .body((output_text[0].clone()).into())
        .map_err(Box::new)?;
    return Ok(resp);  
}
```

3. Register AWS ECR and push the docker image on public/private repositories(You should firstly install [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html))
```bash
aws ecr get-login-password --region region | docker login --username AWS --password-stdin aws_account_id.dkr.ecr.region.amazonaws.com
# In codespace with devcontainer, you should use docker login -u AWS -p $(aws ecr get-login-password --region us-east-1) xxxxx.dkr.ecr.us-east-1.amazonaws.com/w10-rustbert-lambda


docker build -t translate .
docker tag <image hash> <ECR Repo URL>:latest
docker push <ECR Repo URL>:latest
```
![alt text](image-1.png)
![alt text](image-4.png)

4. Deploy the lambda function with Container image(AWS ECR)
![alt text](image-3.png)
![alt text](image-5.png)

## Deliverable
This lambda function can be tested with cURL or aws extension of VSCode. You set the text field as the Chinese you want to translate, and then you will receive the English version. `curl -H "Content-Type: application/json" --data '{"text": "你好"}' https://jme7tsjdtrt3kc5pgvzihphjva0fkudx.lambda-url.us-east-1.on.aws/`

![alt text](image-6.png)



## Reference
[1] https://github.com/nogibjj/rust-pytorch-gpu-template

[2] https://docs.rs/rust-bert/latest/rust_bert/index.html#modules

[3] https://docs.aws.amazon.com/AmazonECR/latest/public/docker-push-ecr-image.html



