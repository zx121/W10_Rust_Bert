use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};
use serde_json::from_str;
use rust_bert::pipelines::translation::{Language, TranslationModelBuilder};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RequestBody {
    pub text: String,
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let body = event.body();
    let s = std::str::from_utf8(body).expect("invalid utf-8 sequence");

    let item = match from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Body not provided correctly: ".to_string() + &err.to_string() + "\n").into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    let model = TranslationModelBuilder::new()
        .with_source_languages(vec![Language::ChineseMandarin])
        .with_target_languages(vec![Language::English])
        .create_model()?;

    let input_text = item.text;
    let output_text = model.translate(&[input_text], None, Language::English)?;

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/plain")
        .body((output_text[0].clone()).into())
        .map_err(Box::new)?;
    return Ok(resp);  
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // process sub pub with tracing. Used to log
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}